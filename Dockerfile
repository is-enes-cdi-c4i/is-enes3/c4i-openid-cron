FROM node:14

WORKDIR /usr/src/app
COPY index.ts ./index.ts
COPY src ./src
COPY .eslintrc.js ./.eslintrc.js
COPY .prettierrc ./.prettierrc
COPY package.json ./package.json
COPY package-lock.json ./package-lock.json
COPY tsconfig.json ./tsconfig.json

# Install dependencies
RUN npm ci --only=production
# Compile typescript
RUN npm run tsc

CMD [ "node", "index.js" ]
