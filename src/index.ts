import * as k8s from '@kubernetes/client-node';
import redis, { Callback } from 'redis';
import { promisify } from 'util';

const NAMESPACE = 'swirrl';

const redisClient = redis.createClient({ host: 'redis', port: 80 });
// eslint-disable-next-line @typescript-eslint/unbound-method
const getAsync = promisify(redisClient.get).bind(redisClient);
// eslint-disable-next-line @typescript-eslint/unbound-method
const smembersAsync = promisify(redisClient.smembers).bind(redisClient);
const srem1: (key: string, arg1: string, cb?: Callback<number>) => boolean =
  redisClient.srem;
// eslint-disable-next-line @typescript-eslint/unbound-method
const srem1Async = promisify(srem1).bind(redisClient);
const del1: (key: string, cb?: Callback<number>) => boolean = redisClient.del;
// eslint-disable-next-line @typescript-eslint/unbound-method
const del1Async = promisify(del1).bind(redisClient);

const kc = new k8s.KubeConfig();
kc.loadFromDefault();

const k8sCoreApi = kc.makeApiClient(k8s.CoreV1Api);

const hasPodOrSession = async (sessionID: string): Promise<boolean> => {
  const expressId = Buffer.from(sessionID)
    .toString('base64')
    .replace(/=+$/, '');
  const podResponse = await k8sCoreApi
    .listNamespacedPod(NAMESPACE)
    .catch((err) => console.error(err));
  if (podResponse) {
    const pods = podResponse.body.items;
    const hasPod = pods.some(
      (pod) =>
        pod.metadata &&
        pod.metadata.labels &&
        pod.metadata.labels.expressId === expressId
    );
    if (hasPod) return true;
  }
  const redisRes = await getAsync(`sess:${sessionID}`);
  if (redisRes) return true;
  return false;
};

const main = async (): Promise<void> => {
  const deleted: string[] = [];
  const sessionIDs = await smembersAsync('sessions');
  for (const sessionID of sessionIDs) {
    if (!(await hasPodOrSession(sessionID))) {
      await del1Async(`refresh:${sessionID}`);
      await srem1Async('sessions', sessionID);
      deleted.push(sessionID);
    }
  }
  console.log('Deleted refresh tokens for sessions:');
  console.log(deleted);
  process.exit();
};

main().catch((err) => console.log(err));
